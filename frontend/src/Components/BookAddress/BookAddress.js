import React from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { useSelector, useDispatch } from 'react-redux';
import { ManageBookAddress } from '../Actions/ManageBookAddress';
import BookAddressList from '../BookAddressList/BookAddressList';
import { Container } from '@material-ui/core';
import BookAddressDashbord from '../BookAddressDashbord/BookAddressDashbord';
const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
        background: '#0078d4'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dialog: {
        display: 'flex',
        marginTop: '30px',
    },
    paper: {
        width: '45%',
        margin: '0px auto',
        padding: '0px auto',
        minHeight: '500px'
    },
    sectionApp: {
        width: '90%',
        position: 'relative',
        left: '10%',
        display: 'flex',
        top: '5%',
    },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const BookAddress = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
        let payload = {
            show: false
        };
        dispatch(ManageBookAddress(payload));
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    useSelector(state => {
        console.log(open);
        if (!open && state.ManageBookAddress.show) {
            handleClickOpen();
        }
        if (state.ManageBookAddress.needUpdate){
            let payload = {
                needUpdate: true
            };
            dispatch(ManageBookAddress(payload));
        }
    });
    return (
        <Auxiliary>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Contacts
            </Typography>
                    </Toolbar>
                </AppBar>
                <Container>
                    <div className={classes.dialog}>
                        <BookAddressList show={true}/>
                        <BookAddressDashbord />
                    </div>
                </Container>
            </Dialog>
        </Auxiliary>
    );
};
export default BookAddress;