import React from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import Mailbody from '../Mailbody/Mailbody';
import Maillist from '../Maillist/Maillist';
import { makeStyles } from '@material-ui/core/styles';
import BookAddress from '../BookAddress/BookAddress';
import AutoReply from '../AutoReply/AutoReply';
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
    },
    mailBody: {
        width: '70%',
    },
    mailList: {
        width: '30%',
    },
}));

const Taskbar = props => {
    console.log(props);
    const classes = useStyles();
    return (
        <Auxiliary>
            <div className={classes.root}>
                <div className={classes.mailList}>
                    <Maillist emailsData={props.emailsData} emailType={props.emailType} />
                </div>
                <div className={classes.mailBody}>
                    <Mailbody />
                </div>
            </div>
            <BookAddress/>
            <AutoReply/>
        </Auxiliary>
    )
}
export default Taskbar;