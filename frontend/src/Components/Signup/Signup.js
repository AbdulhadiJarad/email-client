import React from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { makeStyles } from '@material-ui/core/styles';
import '../Signup/Signup.module.css';
import { Paper, CssBaseline, Typography, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Signin from '../Signin/Signin';
import { Link, Route, BrowserRouter } from 'react-router-dom';
const Background = "https://images.unsplash.com/uploads/141103282695035fa1380/95cdfeef?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb";
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        height: '100vh',
        backgroundImage: `url(${Background})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        width: '400px',
        height: '450px',
        position: 'absolute',
        top: '22%',
        left: '50%',
    },
    elements: {
        display: 'block',
        margin: theme.spacing(1.8, 10, 2),  
    },
    signInText: {
        textAlign: 'center',
        paddingTop: '10px'
    },
    submat: {
        marginLeft: '40px'
    },
    agreeCheck: {
        marginLeft: '40px'
    },
    link:{
        textDecoration:'none'
    }
}));
const Signup = props => {
    const classes = useStyles();
    // const [getStateValidaty, setStateValidaty] = useState({ valid: false });
    return (
        <Auxiliary>
            <BrowserRouter>
                <CssBaseline />
                <div className={classes.root} xs={false} sm={4} md={7}>
                    <Paper className={classes.paper}>
                        <Typography className={classes.signInText} variant="h4">Sign Up !</Typography>
                        <form className="formContainer" noValidate autoComplete="off">
                            <div className={classes.elements}>
                                <TextField fullWidth required id="standard-required" label="Required" defaultValue="Full Name" />
                            </div>
                            <div className={classes.elements}>
                                <TextField fullWidth required id="standard-required" label="Required" defaultValue="Telephone" />
                            </div>
                            <div className={classes.elements}>
                                <TextField fullWidth required id="standard-required" label="Required" type="password" defaultValue="Password" />
                            </div>
                            <div className={classes.elements}>
                                <TextField fullWidth required id="standard-required" label="Required" type="password" defaultValue="Password" />
                                <Typography style={{marginTop:'10px'}} variant="body2">Already Have Account? <Link exact to='/Signin'>Sign In</Link></Typography>
                            </div>
                            <FormControlLabel
                                className={classes.agreeCheck}
                                control={<Checkbox name="checkedA" />}
                                label="I Agree To Damascus University Laws"
                            />
                            <Button className={classes.submat} variant="contained" color="secondary">
                            <Link className={classes.link} exact to='/Signin'>Submate</Link>
                        </Button>
                        </form>
                    </Paper>
                </div>
                <Route exact path="/Signin"  component={Signin} />
            </BrowserRouter>
        </Auxiliary>
    );
}
export default Signup;