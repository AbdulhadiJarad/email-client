export const NewMessage = (props)=>{
    console.log(props);
    return {
        type: 'NEW',
        newMessage:props.newMessage,
        replyTo:props.replyTo,
        messageId: props.messageId,
        subject: props.subject
    };
};