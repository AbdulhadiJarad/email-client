import React from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Auxiliary from '../Auxiliary/Auxiliary';
import { BrowserRouter } from 'react-router-dom';
import Signin from '../Signin/Signin';
function App() {
  return (
    <Auxiliary>
      <BrowserRouter>
              <Signin/>
        {/* <Navbar />
        <Seminav />
        <Sidenav /> */}
        <CssBaseline />
      </BrowserRouter>
    </Auxiliary>
  );
}
export default App;