import React, { useState } from 'react';
import Taskbar from '../Taskbar/Taskbar';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import StarBorder from '@material-ui/icons/StarBorder';
import Auxiliary from '../Auxiliary/Auxiliary';
import Axios from 'axios';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import { useSelector, useDispatch } from 'react-redux';
import { managingMessage } from '../Actions';
import { ClearMessage } from '../Actions/ClearMessage';
import { ManageBookAddress } from '../Actions/ManageBookAddress';
import { ManageAutoReply } from '../Actions/ManageAutoReply';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
    },
    listItem: {
        marginTop: '50px',
        width: '200px',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    drawerPaper: {

    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));
const Sidenav = props => {
    const dispatch = useDispatch();
    // const [open, setOpen] = React.useState(true);
    const [getEmailTarget, setEmailTarget] = useState({ name: "ALL", date: null });
    const [getEmailsList, setEmailsList] = useState({ list: null });

    const classes = useStyles();
    // const handleClick = () => {
    //     setOpen(!open);
    // };
    const clearBody = () => {
        let payload = {
            messageType: null,
            messageID: null,
            from: null,
            avatarColor: null
        };
        console.log(payload);
        dispatch(managingMessage(payload));
    };
    useSelector(state => {
        let observed = false;
        if (state.MessageManaging.deleted && !observed) {
            observed = true;
            let deleted = getEmailsList.list;
            console.log(deleted, typeof deleted);
            let deletedID = state.MessageManaging.deletedID;
            deleted.forEach((info, index) => {
                console.log(info.messageID, deletedID);
                if (info.messageID === deletedID) {
                    deleted.splice(index, 1);
                    return;
                }
            });
            // deleted.splice
            console.log(state);
            console.log(deleted);
            let payload = { deleted: false };
            setEmailsList({ list: deleted });
            dispatch(ClearMessage(payload));
        }
        if (state.MessageManaging.selectedDate && getEmailTarget.searchDate !== state.MessageManaging.selectedDate) {
            let getEmailTargetCopied = Object.assign({}, getEmailTarget);
            setEmailTarget({ searchDate: state.MessageManaging.selectedDate, name: getEmailTargetCopied.name });
        }
        return state.data;
    });
    const handleEmailType = async (name, refresh) => {
        console.log(name);
        setEmailTarget({ name: name });
        clearBody();
        const processTime = setInterval(async () => {
            const getOfflineEmails = await Axios.post("/api/getList", {
                searchType: name,
                searchDate: getEmailTarget.searchDate,
                refresh
            });
            console.log(getOfflineEmails.data);
            if (getOfflineEmails.data.status === 200) {
                setEmailsList({ list: getOfflineEmails.data.emailsOfflineData });
                clearInterval(processTime);
            }
        }, 8000);
    };
    const notifyBookAddress = () => {
        let payload = {
            show: true
        };
        console.log(payload);
        dispatch(ManageBookAddress(payload));
    };
    const notifyAutoReply = () => {
        let payload = {
            showAutoReply: true
        };
        console.log(payload);
        dispatch(ManageAutoReply(payload));
    }
    return (
        <Auxiliary>
            <div className={classes.root}>
                <List
                    className={classes.listItem}
                    component="nav"
                    aria-labelledby="nested-list-subheader"
                    subheader={
                        <ListSubheader component="div" id="nested-list-subheader">
                            Nested List Items
                        </ListSubheader>
                    }
                >
                    <ListItem button onClick={() => handleEmailType("ALL", true)}>
                        <ListItemText primary="Refesh" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("ALL")}>
                        <ListItemIcon>
                            <AllInboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Inbox" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("UNSEEN")}>
                        <ListItemIcon>
                            <AllInboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Un Seen    " />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("SEEN")}>
                        <ListItemIcon>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText primary="Seen" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("[Gmail]/Sent Mail")}>
                        <ListItemIcon>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText primary="Sent" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("[Gmail]/Drafts")}>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Drafts" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("DELETED")}>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Deleted" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("[Gmail]/Starred")}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Starred" />
                    </ListItem>
                    <ListItem button onClick={() => handleEmailType("[Gmail]/Spam")}>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Spam" />
                    </ListItem>
                    <ListItem button onClick={notifyBookAddress}>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Contacts" />
                    </ListItem>
                    <ListItem button onClick={notifyAutoReply}>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Auto Reply" />
                    </ListItem>
                </List>
                <div style={{ marginTop: 100, width: '90%' }}>
                    <Taskbar emailsData={getEmailsList.list} emailType={getEmailTarget.name} />
                </div>

            </div>

        </Auxiliary>
    )
}
export default Sidenav;