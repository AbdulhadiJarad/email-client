import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import ToolBar from '@material-ui/core/ToolBar';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Auxiliary from '../Auxiliary/Auxiliary';
import Axios  from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { NewMessage } from '../Actions/NewMessage';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {SearchMessageServer} from '../Actions/SearchMessageServer';
import {ClearMessage} from '../Actions/ClearMessage';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
const useStyles = makeStyles(theme => ({
    appBar: {
        background: '#f3f2f1',
        height: '50px',
        justifyContent: 'center',
    },
    messageButton: {
        background: '#0078d4'
    },
    details: {
        color: '#0078d4',
        fontSize: '15px',
        textTransform: 'capitalize'
    }
}));
const Seminav = props => {
    // The first commit of Material-UI
   
  const [selectedDate, setSelectedDate] = React.useState(new Date('2020-05-18T21:11:54'));
  const [getCurrentMessageID, setCurrentMessageID] = useState({ID:null,type:null,uid: null});
  useEffect(()=>{
    console.log(selectedDate);
}); 
  const handleDateChange = (date) => {
    alert(date);
    console.log(date);
    setSelectedDate(new Date(date));
    console.log(date);
    const coppiedState = new Date(date.valueOf());
        let payload = {
            selectedDate:coppiedState
        };
        dispatch(SearchMessageServer(payload));
    
    console.log(selectedDate);
  };
  useSelector(state => {
    console.log(state.MessageManaging);
    if (getCurrentMessageID.ID !== state.MessageManaging.messageID) {
        console.log(state);
        setCurrentMessageID({ ID: state.MessageManaging.messageID, messageType: state.MessageManaging.messageType, uid: state.MessageManaging.uid });
    }
    return state.data;
});
const dispatch = useDispatch();
  const handleMessage = async (queryType) =>{
      console.log(queryType);
      console.log(getCurrentMessageID);
  
    let hi = await Axios.post('/api/deleteMessage',{
        messageID: getCurrentMessageID.ID,
        queryType,
        messageType:getCurrentMessageID.messageType,
        uid: parseInt(getCurrentMessageID.uid)
    });
    console.log(hi);
    console.log(hi.data);
    if (hi.data.message===200 && queryType==='DELETED'){
        let payload = {deleted:true};
        dispatch(ClearMessage(payload));
    }
  };
   
    const classes = useStyles();
    let details = null;
    const handleNewMessage = () =>{
        let payload = {
            newMessage:true 
        };
        dispatch(NewMessage(payload));
    };
    if (props.details || true) {
        details = (
            <Auxiliary>
                <span style={{ marginLeft: 50 }} />
                <Button onClick={()=>handleMessage('DELETED')} className={classes.details}>Delete</Button>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="MM/dd/yyyy"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </MuiPickersUtilsProvider>
            </Auxiliary>
        );
    }
    return (
        <Auxiliary>
            <AppBar className={classes.appBar} style={{ marginTop: 50 }}>
                <ToolBar>
                    <Button onClick={handleNewMessage} className={classes.messageButton} variant="contained" color="primary">
                        New Message
                    </Button>
                    {details}
                </ToolBar>
            </AppBar>
        </Auxiliary>
    )
}
export default Seminav;