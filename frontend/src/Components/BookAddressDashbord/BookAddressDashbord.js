import React, { useState } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import { useSelector } from 'react-redux';
import Axios from 'axios';
const useStyles = makeStyles((theme) => ({
    addIconInside: {
        width: '20px',
        marginTop: '50px',
        cursor: 'pointer'
    },
    addIconInsideGroup: {
        width: '20px',
        marginTop: '50px',
        cursor: 'pointer'
    },
    paper: {
        width: '45%',
        margin: '0px auto',
        padding: '0px auto',
        minHeight: '500px'
    },
    note: {
        position: "relative",
        left: '40%',
        top: '36%'
    },
    avatarHandedDisplayed: {
        borderRadius: '50%',
        width: '60px',
        textAlign: 'center',
        primary: 'lime',
        // paddingLeft: '36px',
        fontWeight: 'bold',
        display: 'block',
        margin: '25px',
        paddingTop: '10px',
        fontSize: '34px',
        height: '60px'
    },
    betweenContainer: {
        marginTop: '20px',
        width: '100%',
        margin: '0px auto',
        textAlign: 'center'
    },
    betweenButton: {
        background: '#0078d4',
    },
    usernameAvatar: {
        fontWeight: 'bold',
        paddingTop: '4px',
        display: 'block',
        paddingLeft: '50px',
        fontSize: '22px',
    },
    styleItems: {
        marginLeft: '5%',
        marginTop: 3,
        marginBottom: 3,
        paddingRight: 15,
    }
}));
const BookAddressDashbord = props => {

    const classes = useStyles();
    const [getSelectedContact, setSelectedContact] = useState({ contact: null });
    let contactArray = [];
    let groupArray = [];
    let nickname = '';
    let address = '';
    let name = '';
    useSelector(state => {
        console.log(state);
        if (state.ManageBookAddress.selectedContact && (!getSelectedContact.contact || (getSelectedContact.contact && getSelectedContact.contact.username !== state.ManageBookAddress.selectedContact.username))) {
            console.log(state.ManageBookAddress.selectedContact);
            const selected = Object.assign({}, state.ManageBookAddress.selectedContact);
            console.log(selected);
            if (state.ManageBookAddress.selectedContact.phones.length === 0 || !state.ManageBookAddress.selectedContact.phones)
                selected.phones = [];
            if (state.ManageBookAddress.selectedContact.groups.length === 0 || !state.ManageBookAddress.selectedContact.groups)
                selected.groups = [];
            setSelectedContact((prevState, props) => {
                return {
                    contact: selected
                };
            });
        }
        return state.ManageBookAddress.selectedContact;
    });
    const addNewContactInfo = async () => {
        const coppiedState = Object.assign({}, getSelectedContact.contact);
        const setContact = await Axios.post("/api/newContact", {
            newContact: coppiedState
        });
        console.log(setContact)
        if (setContact.data.message === 200) {
            // handleDiscard();
        } else {
            console.log('smth went wrong');
        }
    };
    const handleTextFieldChange = async (code, value) => {
        value = value.replace(/\r?\n|\r/g, '');
        console.log(value);
        console.log(getSelectedContact);
        const coppiedState = Object.assign({}, getSelectedContact.contact);
        if (code === 100) {
            coppiedState.username = value;
        } else if (code === 200) {
            coppiedState.nickname = value;
        } else if (code === 400) {
            coppiedState.phones.push(value);
        } else if (code === 300) {
            coppiedState.groups.push(value);
        } else if (code === 500) {
            coppiedState.address = value;
        }
        setSelectedContact({ contact: coppiedState }, () => {
            console.log(getSelectedContact);
        });
    };
    let viewer = null;
    if (!getSelectedContact.contact) {
        viewer = <img alt={'addicon'} className={classes.note} style={{ width: '20%' }} src="https://image.flaticon.com/icons/svg/431/431280.svg"></img>;
    }
    else if (getSelectedContact.contact) {
        console.log(getSelectedContact.contact);
        if (getSelectedContact.contact.nickname) {
            nickname = (
                <>
                    <p className={classes.styleItems}>Nickname: </p>
                    <Chip className={classes.styleItems} label={getSelectedContact.contact.nickname} />
                </>
            );
        }
        if (getSelectedContact.contact.username) {
            name = (
                <>
                    <p className={classes.styleItems}>Name: </p>
                    <Chip className={classes.styleItems} label={getSelectedContact.contact.username} />
                </>
            );
        }
        if (getSelectedContact.contact.phones.length > 0) {
            contactArray.push(
                <p className={classes.styleItems}>Phones: </p>
            );
            for (let i = 0; i < getSelectedContact.contact.phones.length; i++) {
                contactArray.push(
                    <Chip className={classes.styleItems} label={getSelectedContact.contact.phones[i]} />
                );
            }
        }
        if (getSelectedContact.contact.groups.length > 0) {
            groupArray.push(<p className={classes.styleItems}>Groups: </p>);
            for (let i = 0; i < getSelectedContact.contact.groups.length; i++) {
                groupArray.push(<Chip clickable className={classes.styleItems} label={getSelectedContact.contact.groups[i]} />);
            }
        }
        if (getSelectedContact.contact.address) {
            address = (
                <>
                    <p className={classes.styleItems}>Address: </p>
                    <Chip className={classes.styleItems} label={getSelectedContact.contact.address} />
                </>
            );
        }

        viewer = (
            <Auxiliary>
                <div className={classes.sectionApp}>
                    <div style={{ backgroundColor: getSelectedContact.contact.uiColor }} className={classes.avatarHandedDisplayed}>
                        {getSelectedContact.contact.username[0]}
                    </div>
                    <div className={classes.usernameAvatar}>
                        {getSelectedContact.contact.username}
                    </div>
                </div>
                <Divider className={classes.divider} />
                <div>
                    {name}
                    {nickname}
                    {contactArray}
                    {groupArray}
                    {address}
                </div>
                <TextField
                    style={{ marginLeft: '5%', paddingRight: 15, width: '40%' }}
                    multiline
                    label="Nick Name"
                    disabled={getSelectedContact.contact.nickname != null}
                    name="contact"
                    value={getSelectedContact.contact.nickname}
                    placeholder="Add Nick Name"
                    onKeyUp={(event) => {
                        if (event.key === 'Enter')
                            handleTextFieldChange(200, event.target.value)
                    }}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    style={{ marginLeft: '5%', paddingRight: 15, width: '40%' }}
                    multiline
                    label="Group"
                    name="contact"
                    placeholder="Add Group"
                    onKeyUp={(event) => {
                        console.log(event.key);
                        if (event.key === 'Enter')
                            handleTextFieldChange(300, event.target.value)
                    }}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    style={{ marginLeft: '5%', paddingRight: 15, width: '40%' }}
                    multiline
                    label="Phone No:"
                    placeholder="New Phone: "
                    name="contact"
                    onKeyUp={(event) => {
                        if (event.key === 'Enter')
                            handleTextFieldChange(400, event.target.value)
                    }}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    style={{ marginLeft: '5%', paddingRight: 15, width: '40%' }}
                    multiline
                    label="Address:"
                    placeholder="Type Address"
                    name="contact"
                    onKeyUp={(event) => {
                        if (event.ctrlKey && event.key === 'Enter')
                            handleTextFieldChange(500, event.target.value)
                    }}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />

                <div className={classes.betweenContainer}>
                    <Button onClick={addNewContactInfo} className={classes.betweenButton} variant="contained" color="primary">
                        Save
                      </Button>
                </div>

            </Auxiliary>
            //read body api us invoked immediatly
        );
    }
    return (
        <Auxiliary>
            <Paper style={{ maxHeight: 580, overflow: 'auto' }} className={classes.paper}>
                {viewer}
            </Paper>
        </Auxiliary>
    )
};
export default BookAddressDashbord;