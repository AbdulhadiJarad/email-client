import React, { useState, useEffect } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { useDispatch } from 'react-redux';
import { LoggingAuthentication } from '../Actions/LoggingAuthentication';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, CssBaseline, Typography, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Navbar from '../Navbar/Navbar';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';
import Sidenav from '../Sidenav/Sidenav';
import { Link, Route, BrowserRouter } from 'react-router-dom';
import Seminav from '../Seminav/Seminav';
import Signup from '../Signup/Signup';
const Background = "https://images.unsplash.com/uploads/141103282695035fa1380/95cdfeef?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb";
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        height: '100vh',
        backgroundImage: `url(${Background})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        width: '400px',
        height: '300px',
        position: 'absolute',
        top: '22%',
        left: '50%',
    },
    elements: {
        display: 'block',
        margin: theme.spacing(1.8, 10, 2),
    },
    signInText: {
        textAlign: 'center',
        paddingTop: '10px'
    },
    submat: {
        marginLeft: '80px',
        marginTop: '20px'
    },
    link:{
        textDecoration:'none'
    }
}));

const Signin = props => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [getUser,setUser] = useState({email:null,password:null});
    const [valid,setValid] = useState(false);
    const handleSubmate = async()=>{
        const loggin = await Axios.post("/api/login",{  
            email:getUser.email,
            password:getUser.password
          });
          console.log();
        if (loggin.data.message==="404"){
            setValid(false);
            console.log('does not exist ');
        }
        else if (loggin.data.message === "200") {
            setValid(true);
            let payload = {
                email:getUser.email,
                password:getUser.password
            };
            console.log(payload);
            dispatch(LoggingAuthentication(payload));
            
        }
    };
    useEffect(()=>{
        // console.log(getUser);
    });
    const handleChange = (ev)=>{
        const name = ev.target.name;
        const value = ev.target.value;
        const obj = Object.assign({},getUser);
        if (name==="email")
            setUser({email:value,password:obj.password});
        else if (name==="password")
            setUser({email:obj.email,password:value});
        console.log(getUser);
    }
    return (
        <Auxiliary>
            <BrowserRouter>
                <CssBaseline />
            
                <Route exact path="/signin" component={Signin} >
                <div className={classes.root} xs={false} sm={4} md={7}>
                    <Paper className={classes.paper}>
                        <Typography className={classes.signInText} variant="h4">Sign In !</Typography>
                        <form className="formContainer" noValidate autoComplete="off">
                            <div className={classes.elements}>
                                <TextField onChange={handleChange} name="email" fullWidth required id="standard-required" label="Required" defaultValue="User Name" />
                            </div>
                            <div className={classes.elements}>
                                <TextField onChange={handleChange} name="password" fullWidth required id="standard-required" label="Required" type="password" defaultValue="Password" />
                            </div>
                            
                            <Button style={{backgroundColor:'#e2e4e1'}} onClick={handleSubmate} className={classes.submat} variant="contained" color="secondary">
                                <Link onClick={handleSubmate} to={'/dashbord'} className={classes.link}>Sign In</Link>
                            </Button>
                        </form>
                    </Paper>
                </div>
                </Route>
                {valid?(<><Route exact path="/dashbord" component={Navbar} />
                <Route exact path="/dashbord" component={Seminav} />
                <Route exact path="/dashbord" component={Sidenav} /></>):null}
                <Route exact path="/signup" component={Signup} />
            </BrowserRouter>
        </Auxiliary>
    );
}
export default withRouter(Signin);