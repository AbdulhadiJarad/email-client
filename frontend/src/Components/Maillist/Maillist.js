import React from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {  ListItemText, ListSubheader,  LinearProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { managingMessage } from '../Actions';
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: '1',
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    headerStyle: {
        color: '#288ddb',
    },
    singleUnreadedMessage: {
        flexGrow: '1',
        width: '100%',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        height: '80px !important'
    },
    headerSeenStyle: {
        color: 'black'
    },
    attachmentIcon:{
        disaply:'inlne',
        marginRight: '5px',
        marginTop: '10px',
        height: '20px',
        width: '20px'
    },
    body: {
        marginRight: '20px',
        paddingRight: '20px'
    },
    avatarHanded: {
        borderRadius: '50%',
        width: '40px',
        primary: 'lime',
        fontSize: '18px',
        paddingTop: '10px',
        paddingLeft: '14px',
        height: '40px'
    }
}));
let payload = {};
const Maillist = props => {

    const dispatch = useDispatch();
    console.log(props);
    let emailType = props.emailType;
    if (props.emailType === 'ALL')
        emailType = 'INBOX';
    const classes = useStyles();
    const [getSearchedTextState, setSearchedTextState] = useState({ target: '-1' });
    let tempList = [];
    const readMessage = (message) => {
        payload = {
            messageType: message.messageType,
            messageID: message.messageID,
            from: message.from.value[0].address,
            uid: message.uid,
            messageSubject: message.subject,
            cc: message.cc,
            bcc: message.bcc,
            to: (message.to ? message.to.text : 'Un known' ),
            date: message.date,
            hours: message.hours,
            messageId: message.messageId,
            // replyTo: message.replyTo.text,
            avatarColor: message.avatarColor,
        };
        console.log(payload);
        message.seen = true;
        dispatch(managingMessage(payload));
    };
    useSelector(state => {
        console.log(state);
        if (state.SearchManaging.target !== getSearchedTextState.target) {
            setSearchedTextState({ target: state.SearchManaging.target });
        }
        return state.SearchManaging.target;
    });
    function isMatchCase(message, target){
        console.log(message,target);
        if (!target){
            return true;
        }
        if (message.subject){
            if (message.subject.toLowerCase().includes(target.toLowerCase())){
                return true;
            }
        }
        console.log(message.from);
        if (message.from.value[0].name){
            if (message.from.text.toLowerCase().includes(target.toLowerCase())){
                return true;
            }
        }
        if (message.text){
            if (message.text.toLowerCase().includes(target.toLowerCase())){
                return true;
            }
        }
        return false;
    }
    if (props.emailsData) {
        props.emailsData.forEach((message, index) => {
            if (isMatchCase(message,getSearchedTextState.target)) {
                tempList.push(
                    <Auxiliary key={message.messageId}>
                        <ListItem onClick={() => readMessage(message)} button={true} className={message.seen ? classes.singleReadedMessage : classes.singleUnreadedMessage} alignItems="flex-start">
                            <ListItemAvatar>
                                <div style={{ backgroundColor: message.avatarColor }} className={classes.avatarHanded}>
                                    {message.messageType === '[Gmail]/Sent Mail' ? message.to.text[0].toUpperCase() : message.from.text[0].toUpperCase()}
                                </div>
                            </ListItemAvatar>
                            {message.hasAttachment? <img alt={'attachment'} className={classes.attachmentIcon} src={'./attach.svg'}/>:null}
                            <ListItemText
                                primary={message.from.value[0].name ? message.from.value[0].name : message.from.value[0].address}
                                secondary={
                                    <React.Fragment>
                                        <Typography
                                            component="span"
                                            variant="body2"
                                            className={classes.headerStyle}
                                        >
                                            {message.subject}
                                        </Typography>
                                        <Typography className={classes.body} component={'يهر'} display="block" variant="body2">
                                            {message.text}
                                        </Typography>
                                    </React.Fragment>
                                }
                            />
                        </ListItem>
                        <Divider />
                    </Auxiliary>
                );
            }
        });
    }
    return (
        <Auxiliary>
            <Paper style={{ maxHeight: 530, overflow: 'auto' }}>
                <List
                    component="nav"
                    aria-labelledby="nested-list-subheader"
                    subheader={
                        <ListSubheader component="div" id="nested-list-subheader">
                            {emailType}
                            {(props.emailsData === null || props.emailsData === null === undefined) ? <LinearProgress /> : null}
                        </ListSubheader>
                    }
                    className={classes.root}>
                    {tempList}
                </List>
            </Paper>
        </Auxiliary>
    )
}
export default Maillist;