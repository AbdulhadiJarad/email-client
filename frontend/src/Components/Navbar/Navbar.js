import React,{useState} from 'react';
import {  makeStyles } from '@material-ui/core/styles';
import ToolBar from '@material-ui/core/ToolBar';
import AppBar from '@material-ui/core/AppBar';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import Signin from '../Signin/Signin';
import { Link, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { SearchManagingAction } from '../Actions/SearchManagingAction';
import { LoggingAuthentication } from '../Actions/LoggingAuthentication';
const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1,
    },
    nav: {

    },
    appBar: {
        height: "50px",
        width: '100%',
        justifyContent: 'center',
        backgroundColor:'#0078d4'
    },
    textField: {
        position: 'relative',
        background: 'white',
        height: '40px'
    },
    search: {
        background: "#b3d7f2",
        margin: "0px 0px 0px 15%",
        width: "320px",
        padding: theme.spacing(0, 0, 0, 1),
        borderRadius: "2px",
    },
    searchIcon: {
        marginLeft: '-30px',
        height: '100%',
        position: 'relative',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    link: {
        textDecoration: 'none'
    }
}));
function Navbar() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [getSearchedText,setSearchedText] = useState({target:null});
    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };
    const searchOnMessages = (ev)=>{
        console.log(ev.target.value);
        const coppiedState = Object.assign({},getSearchedText);
        coppiedState.target = ev.target.value;
        setSearchedText({target:coppiedState.target});
        dispatch(SearchManagingAction(ev.target.value));
    };
    const isMenuOpen = Boolean(anchorEl);
    const handleMenuClose = () => {
        setAnchorEl(null);
    };
    const handleSignOut = () =>{
        let payload = {
            email:null,
            password:null
        };
        dispatch(LoggingAuthentication(payload));
    };
    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleMenuClose}>
                <Link className={classes.link} onClick={handleSignOut} to='/Signin'>Sign out</Link>
            </MenuItem>
        </Menu>
    );
    return (
        <React.Fragment>
            <AppBar position="static" className={classes.appBar}>
                <ToolBar className={classes.nav}>
                    <InputBase
                        placeholder="Search…"
                        className={classes.search}
                        onChange={searchOnMessages}
                        inputProps={{ 'aria-label': 'search' }}
                    >

                    </InputBase>
                    <div className={classes.searchIcon}>
                        <SearchIcon />
                    </div>
                    <div className={classes.grow} />
                    <div className={classes.icons} >
                        <IconButton
                            edge="end"
                            aria-label="account of current user"
                            aria-haspopup="true"
                            color="inherit"
                            onClick={handleProfileMenuOpen}
                        >
                            <AccountCircle />
                        </IconButton>
                    </div>
                    {renderMenu}
                </ToolBar>
            </AppBar>
            <Route exact path="/Signin" component={Signin} />
        </React.Fragment>
    );
}

export default Navbar;