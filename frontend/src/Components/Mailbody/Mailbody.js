import React, { useEffect } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, CircularProgress, Button } from '@material-ui/core';
import ToolBar from '@material-ui/core/ToolBar';
import { useSelector, useDispatch } from 'react-redux';
import ChipInput from 'material-ui-chip-input';
import { useState } from 'react';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import { NewMessage } from '../Actions/NewMessage';
import ReactHtmlParser from 'react-html-parser';
import Chip from '@material-ui/core/Chip';
import HttpIcon from '@material-ui/icons/Http';
import AttachmentIcon from '@material-ui/icons/Attachment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
    paper: {
        minHeight: '530px',
    },
    header: {
        display: 'flex',
        width: '90%',
        backgroundColor: '#ffffff',
        zIndex: '10'
    },
    avatar: {
        borderRadius: '50%',
        margin: '20px'
    },
    headerText: {
        paddingTop: '20px'
    },
    attachment: {
        // backgroundColor:'#fafafa',
        margin: '4px'
    },
    attachments: {
        // border:  'solid 2px #fafafa'
    },
    bodyText: {
        padding: "20px"
    },
    noSelectImage: {
        width: '150px',
        position: 'relative',
        marginTop: '20%',
        left: '40%'
    },
    attachmentElement:{
        minHeight: '300px',
        maxHeight: '500px',
        minWidth: '300px',
        maxWidth: '500px'
    },
    selectAnItem: {
        textAlign: "center"
    },
    avatarHanded: {
        margin: '20px',
        borderRadius: '50%',
        width: '40px',
        primary: 'lime',
        height: '40px',
        fontSize: '18px',
        paddingTop: '10px',
        paddingLeft: '15px',
    },
    container: {
        marginTop: '130px',
        backgroundColor: '#ffffff'
    },
    subject: {
        wordWrap: 'breakWord'
    },
    print: {
        float: 'right',
        right: '5px',
        width: '100vh'
    },
    textarea: {
        minHeight: '300px',
        width: '97%',
        marginLeft: '8px',
        marginRight: '8px',
        marginTop: '8px'
    },
    toolBar: {
        width: '100%',
        backgroundColor: "#f3f2f1",
        marginTop: '7px'
    },
    sendMessage: {
        background: '#0078d4'
    },
    betweenButton: {
        background: '#0078d4',
        marginLeft: '20px'
    },
    discardButton: {
        marginLeft: '30px'
    }
}));
const Mailbody = props => {
    const dispatch = useDispatch();
    let spinner = false;
    const [getCurrentMessageID, setCurrentMessageID] = useState({ ID: "empty" });
    const [getStatus, setStatus] = useState({ cc: false, bcc: false });
    const [getSubject, setSubject] = useState({ subject: "null" });
    const [getBodyState, setBodyState] = useState({ data: null, from: null, cc: null, bcc: null, to: null, replyTo: null, avatarColor: null, html: false, newMessage: false, hours: null, date: null, attachments: null, messageId: null });
    const [getNewMessage, setNewMessage] = useState({ to: [], subject: null, body: null, cc: [], bcc: [], attachments: [], messageId: null });
    const [getSelectedAttachment, setSelectedAttachment] = useState({ open: false, src: null, type: null, dir: null });
    const [open, setOpen] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleStatus = name => {
        const { cc, bcc } = getStatus;
        if (name === 'bcc')
            setStatus({ cc: cc, bcc: true });
        else if (name === 'cc')
            setStatus({ cc: true, bcc: bcc });
    };
    useEffect(() => {
        console.log(getNewMessage);
        console.log(getBodyState);
    });
    const handleChange = (valueInput, nameInput) => {
        const name = nameInput;
        const value = valueInput;
        console.log(name, value);
        const coppiedState = Object.assign({}, getNewMessage);
        let { to, subject, body, cc, bcc, attachments, isReplyTo, messageId } = coppiedState;
        if (name === 'to')
            setNewMessage({ to: valueInput, subject, body, cc, bcc, attachments, isReplyTo, messageId });
        else if (name === 'subject')
            setNewMessage({ to, subject: value, body, cc, bcc, attachments, isReplyTo, messageId });
        else if (name === 'body')
            setNewMessage({ to, subject, body: value, cc, bcc, attachments, isReplyTo, messageId });
        else if (name === 'cc')
            setNewMessage({ to, subject, body, cc: valueInput, bcc, attachments, isReplyTo, messageId });
        else if (name === 'bcc')
            setNewMessage({ to, subject, body, cc, bcc: valueInput, attachments, isReplyTo, messageId });
        console.log(getNewMessage);
    };

    const handleDiscard = () => {
        let payload = {
            newMessage: false
        };
        dispatch(NewMessage(payload));
        setBodyState({ newMessage: false });
    };
    const handleSendEmail = async () => {
        const coppiedState = { ...getNewMessage };
        console.log(getNewMessage);
        const sendEmail = await Axios.post("/api/sendMessage", {
            newMessage: coppiedState
        });
        console.log(sendEmail);
        if (sendEmail.data.message === 200) {
            handleDiscard();
        } else {
            console.log('email not send');
        }
    };

    const displayAttachment = (info) => {
        setSelectedAttachment({
            src: info,
            type: info.match(/\.[0-9a-z]+$/i)[0],
            open: true
        });
    };
    const handleReplyTo = () => {
        const coppiedState = Object.assign({}, getBodyState);
        console.log(coppiedState);
        let payload = {
            newMessage: true,
            replyTo: coppiedState.from,
            messageId: coppiedState.messageId,
            subject: getSubject.subject
        };
        console.log(payload);
        dispatch(NewMessage(payload));
    };
    const getBody = async (message, searchType) => {
        console.log(message, searchType);
        const getOfflineEmails = await Axios.post("/api/readBody", {
            messageID: message.messageID,
            uid: message.uid,
            searchType,
        });
        console.log(getOfflineEmails);
        setBodyState({ data: getOfflineEmails.data.data, to: message.to, cc: message.cc ? message.cc.text : null, bcc: message.bcc ? message.bcc.text : null, from: message.from, replyTo: message.replyTo, avatarColor: message.avatarColor, html: getOfflineEmails.data.html, emailType: searchType, date: message.date, hours: message.hours, attachments: getOfflineEmails.data.attachments, dir: getOfflineEmails.data.dir, messageId: message.messageId, references: getOfflineEmails.data.references });
        console.log(getBodyState, getBodyState.to);
    };
    const handleCloseAttachment = () => {
        setSelectedAttachment({ open: false });
    }
    const getMessageBody = useSelector(state => {
        console.log(state.MessageManaging);
        if ((state.MessageManaging.newMessage && !getBodyState.newMessage && !state.MessageManaging.replyTo)) {
            setSubject({ subject: null })
            setNewMessage({ to: [], subject: null, body: null, cc: [], bcc: [], attachments: [], isReplyTo: false, messageId: null });
            setBodyState({ newMessage: true });
        } else if (state.MessageManaging.newMessage && !getBodyState.newMessage && state.MessageManaging.replyTo) {
            setNewMessage({ to: state.MessageManaging.replyTo ? [state.MessageManaging.replyTo] : [], subject: state.MessageManaging.subject ? state.MessageManaging.subject : null, body: null, cc: [], bcc: [], attachments: [], isReplyTo: state.MessageManaging.replyTo ? true : false, messageId: state.MessageManaging.messageId });
            setBodyState({ newMessage: true });
        } else if (getCurrentMessageID.ID !== state.MessageManaging.messageID && state.MessageManaging.messageID) {
            getBody(state.MessageManaging, state.MessageManaging.messageType);
            setCurrentMessageID({ ID: state.MessageManaging.messageID });
            setSubject({ subject: state.MessageManaging.messageSubject });
        }
        return state.data;
    });
    console.log(getMessageBody);
    const classes = useStyles();
    let headerText = null;
    let vewier = null;
    let attachments = [];
    if (getCurrentMessageID.ID !== 'empty' && !!getCurrentMessageID.ID && getCurrentMessageID.ID != null && getBodyState.data) {
        console.log(props);
        let body = null;
        console.log(getBodyState);
        if (!getBodyState.html) {
            body = getBodyState.data ? getBodyState.data : '';
        } else {
            spinner = true;
            body = ReactHtmlParser(getBodyState.data);
        }
        if (getBodyState.attachments) {

            getBodyState.attachments.forEach((info, index) => {
                attachments.push(
                    // <Typography style={{ display: ((!getBodyState.attachments) ? 'none' : 'block') }} className={classes.attachment} variant="subtitle2">

                    // </Typography>
                    <Chip clickable onClick={() => displayAttachment(info)} label={info} />
                );
            });
        }
        headerText = (
            <div className={classes.headerText}>
                <Typography variant="subtitle2">
                    {getBodyState.emailType === '[Gmail]/Sent Mail' ? ('To: ' + getBodyState.to) : ('From: ' + getBodyState.from)}
                </Typography>
                <Typography style={{ display: ((!getBodyState.cc) ? 'none' : 'block') }} variant="subtitle2">
                    {'CC: ' + (getBodyState.cc)}
                </Typography>
                <Typography style={{ display: ((!getBodyState.bcc) ? 'none' : 'block') }} variant="subtitle2">
                    {'BCC: ' + (getBodyState.bcc)}
                </Typography>
                <Typography style={{ display: ((!getBodyState.replyTo) ? 'none' : 'block') }} variant="subtitle2">
                    {'Reply To: ' + (getBodyState.replyTo)}
                </Typography>
                <Typography variant="subtitle2">
                    {'Date: ' + getBodyState.date + ' Time: ' + getBodyState.hours}
                </Typography>
                <Typography style={{ display: ((!getSubject.subject) ? 'none' : 'block') }} className={classes.subject} variant="subtitle2">
                    {'Subject: ' + getSubject.subject}
                </Typography>
                <Typography style={{ display: ((!getBodyState.attachments || getBodyState.attachments.length<=0) ? 'none' : 'block') }} className={classes.subject} variant="subtitle2">
                    Attachments:
                </Typography>

                <div className={classes.attachments}>
                    {attachments}
                </div>

                <Dialog
                    fullScreen={fullScreen}
                    open={getSelectedAttachment.open}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle  id="responsive-dialog-title">{"Attachment"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {getSelectedAttachment.src}
                        </DialogContentText>
                        {getSelectedAttachment.type && getSelectedAttachment.type.toLowerCase() === '.pdf' ? <embed className={classes.attachmentElement} src={"http://127.0.0.1:8080/" + getBodyState.dir + "/" + getCurrentMessageID.ID + '/attachments/' + getSelectedAttachment.src} width="800px" height="2100px" /> : null}
                        {getSelectedAttachment.type && (getSelectedAttachment.type.toLowerCase() === '.jpg' || getSelectedAttachment.type.toLowerCase() === '.png') ? <img alt='attachment' className={classes.attachmentElement} src={"http://127.0.0.1:8080/" + getBodyState.dir + "/" + getCurrentMessageID.ID + '/attachments/' + getSelectedAttachment.src} /> : null}
                        {getSelectedAttachment.type && (getSelectedAttachment.type.toLowerCase() === '.mp3' || getSelectedAttachment.type.toLowerCase() === '.ogg' || getSelectedAttachment.type.toLowerCase() === '.m4a') ? <audio controls><source src={"http://127.0.0.1:8080/" + getBodyState.dir + "/" + getCurrentMessageID.ID + "/attachments/" + getSelectedAttachment.src} type="audio/ogg" /></audio> : null}
                        {getSelectedAttachment.type && getSelectedAttachment.type.toLowerCase() === '.mp4' ? <video className={classes.attachmentElement} width="400" controls><source src={"http://127.0.0.1:8080/" + getBodyState.dir + "/" + getCurrentMessageID.ID + "/attachments/" + getSelectedAttachment.src} type="video/mp4" /></video> : null}
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCloseAttachment} color="primary">
                            Cancle
          </Button>

                    </DialogActions>
                </Dialog>
            </div>
        );
        vewier = (
            <Paper style={{ maxHeight: 500, overflow: 'auto' }} className={classes.paper} variant="outlined" elevation={3} >
                <div style={{ position: 'fixed' }} className={classes.header}>

                    <div style={{ backgroundColor: getBodyState.avatarColor }} className={classes.avatarHanded}>
                        {(getBodyState.emailType === '[Gmail]/Sent Mail') ? (getBodyState.to[0].toUpperCase()) : getBodyState.from[0].toUpperCase()}
                    </div>
                    <br />
                    {headerText}
                    <Button onClick={handleReplyTo} color="primary">Reply</Button>

                </div>

                <Container className={classes.container}>
                    {body}
                </Container>
            </Paper>
        )
    } else if (getBodyState.newMessage) {
        vewier = <Paper style={{ maxHeight: 500, overflow: 'auto' }} className={classes.paper} variant="outlined" elevation={3} >
            <ChipInput
                disabled={getNewMessage.isReplyTo}
                placeholder={getNewMessage.isReplyTo ? null : "To"}
                defaultValue={(getNewMessage.to && getNewMessage.isReplyTo) ? [getNewMessage.to] : []}
                allowDuplicates={false}
                style={{ marginLeft: '8px !important', paddingRight: '15 !important', width: '100%', disaply: 'block !important' }}
                onChange={(chip) => handleChange(chip, 'to')}
            />
            {getStatus.cc ? (<ChipInput
                placeholder="cc"
                allowDuplicates={false}
                style={{ marginLeft: '8px !important', paddingRight: '15 !important', width: '100%', disaply: 'block !important' }}
                onChange={(chip) => handleChange(chip, 'cc')}
            />)
                : null}
            {getStatus.bcc ? <ChipInput
                placeholder="bcc"
                allowDuplicates={false}
                style={{ marginLeft: '8px !important', paddingRight: '15 !important', width: '100%', disaply: 'block !important' }}
                onChange={(chip) => handleChange(chip, 'bcc')}
            />
                : null}
            {<TextField
                id="standard-full-width"
                style={{ paddingLeft: 8, paddingRight: 15 }}
                placeholder="Subject"
                disabled={getNewMessage.isReplyTo}
                defaultValue={getNewMessage.isReplyTo ? getNewMessage.subject : null}
                fullWidth
                onChange={(ev) => handleChange(ev.target.value, ev.target.name)}
                name="subject"
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
            />}
            <textarea
                name="body"
                onChange={ev => handleChange(ev.target.value, ev.target.name)}
                className={classes.textarea}
            />
            <ToolBar className={classes.toolBar}>
                <Button onClick={handleSendEmail} className={classes.sendMessage} variant="contained" color="primary">
                    Send Message
                    </Button>
                <Button onClick={() => handleStatus("cc")} className={classes.betweenButton} variant="contained" color="primary">
                    CC
                    </Button>
                <Button onClick={() => handleStatus("bcc")} className={classes.betweenButton} variant="contained" color="primary">
                    BCC
                    </Button>
                <Button onClick={handleClickOpen} className={classes.betweenButton} variant="contained" color="primary">
                    <HttpIcon />
                </Button>
                <Button onClick={() => handleStatus("bcc")} className={classes.betweenButton} variant="contained" color="primary">
                    <AttachmentIcon />
                </Button>
                <Button onClick={handleDiscard} className={classes.discardButton} variant="contained">
                    Discard
                </Button>

            </ToolBar>
        </Paper>
    } else if (spinner) {
        vewier = <Paper style={{ maxHeight: 540, overflow: 'auto' }} className={classes.paper} variant="outlined" elevation={3} >
            <Container className={classes.container}>
                <CircularProgress disableShrink />
            </Container>
        </Paper>
    } else {
        vewier =
            <Paper className={classes.paper}>
                <img alt={'select'} className={classes.noSelectImage} src="https://image.flaticon.com/icons/svg/726/726586.svg" />
                <Typography className={classes.selectAnItem} variant="body1">Select an item to read</Typography>
            </Paper>;
    }
    return (
        <Auxiliary>
            {/* <embed src="http://127.0.0.1:8080/Notes.pdf" width="800px" height="2100px" />
        <img src='http://127.0.0.1:8080/a.jpg' /> */}
            {/* <audio controls>
  <source src="horse.ogg" type="audio/ogg"/>
        </audio>
        <video width="400" controls>
  <source src="mov_bbb.mp4" type="video/mp4"/>
        </video> */}
            <main>

                <div />
                <Container>
                    {vewier}
                </Container>
            </main>

            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Attachment"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add URL Attachment.
          </DialogContentText>
                    <TextField
                        id="standard-full-width"
                        style={{ paddingLeft: 8, paddingRight: 15 }}
                        placeholder="word"
                        fullWidth
                        onChange={handleChange}
                        name="word"
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    /><TextField
                        id="standard-full-width"
                        style={{ paddingLeft: 8, paddingRight: 15 }}
                        placeholder="URL"
                        fullWidth
                        onChange={handleChange}
                        name="url"
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Cancle
          </Button>

                    <Button onClick={handleClose} color="primary" autoFocus>
                        ADD
          </Button>

                </DialogActions>
            </Dialog>
        </Auxiliary>
    );
}

export default Mailbody;