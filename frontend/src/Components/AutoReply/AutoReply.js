import React, { useEffect } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Axios from 'axios';
import Switch from '@material-ui/core/Switch';
import Slide from '@material-ui/core/Slide';
import { useSelector, useDispatch } from 'react-redux';
import { ManageAutoReply } from '../Actions/ManageAutoReply';
import { TextField, Button } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
        background: '#0078d4'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dialog: {
        display: 'flex',
        marginTop: '30px',
    },
    paper: {
        width: '45%',
        margin: '0px auto',
        padding: '0px auto',
        minHeight: '500px'
    },
    sectionApp: {
        width: '90%',
        position: 'relative',
        left: '10%',
        display: 'flex',
        top: '5%',
    },
}));
const Transition = React.forwardRef(function Transition(props, ref) {return <Slide direction="up" ref={ref} {...props} />;});
const AutoReply = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [getInfo, setInfo] = React.useState({ subject: null, body: null });
    const handleClose = () => {
        setOpen(false);
        let payload = {
            show: false
        };
        dispatch(ManageAutoReply(payload));
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleGetData = async () => {
        const getAutoReplyData = await Axios.post("/api/getAutoReplyData", {});
        console.log(getAutoReplyData.data);
        if (getAutoReplyData.data.message === 200) {
            setInfo({ subject: getAutoReplyData.data.subject, body: getAutoReplyData.data.body, enable: getAutoReplyData.data.enable });
        }
    };
    useSelector(state => {
        console.log(open);
        if (!open && state.ManageAutoReply.show) {
            handleClickOpen();
            handleGetData();
        }
    });
    useEffect(() => {
        console.log(getInfo);
    });
    const handleChange = (code, value) => {
        const coppiedState = getInfo;
        console.log(code, value);
        if (code === 100) {
            setInfo({ enable: !value, subject: coppiedState.subject, body: coppiedState.body });
        } else if (code === 200) {
            setInfo({ enable: coppiedState.enable, subject: value, body: coppiedState.body });
        } else if (code === 300) {
            setInfo({ enable: coppiedState.enable, subject: coppiedState.subject, body: value });
        }
    };
    const handleSave = async () => {
        const getAutoReplyData = await Axios.post("/api/AutoReplyManage", {
            enable: getInfo.enable,
            subject: getInfo.subject,
            body: getInfo.body
        });
        console.log(getAutoReplyData.data);
        if (getAutoReplyData.data.message === 200) {
            // handleClose();
        }
    }
    return (
        <Auxiliary>
            <Dialog open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>Auto Reply</Typography>
                        <Switch checked={getInfo.enbale} onChange={ev => handleChange(100, getInfo.enable)} name="checkedA" inputProps={{ 'aria-label': 'secondary checkbox' }} />
                    </Toolbar>
                </AppBar>
                <TextField style={{width:'50%', margin:'0px auto'}} placeholder={'type your Subject'} defaultValue={getInfo.subject} onChange={ev => handleChange(200, ev.target.value)} />
                <TextField style={{width:'50%', margin:'0px auto'}} placeholder={'type your Body'} defaultValue={getInfo.body} onChange={ev => handleChange(300, ev.target.value)} />
                <Button color={'primart'} onClick={handleSave}>Set Message</Button>
            </Dialog>
        </Auxiliary>
    );
};
export default AutoReply;