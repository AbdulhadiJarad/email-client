import React, { useEffect, useState } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import { makeStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import { useSelector, useDispatch } from 'react-redux';
import Axios from 'axios';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { UpdateBookAddress } from '../Actions/UpdateBookAddress';
import { ViewInfo } from '../Actions/ViewContact';
const uiColors = ['#f8bbd0', '#ffcdd2', '#f8bbd0', '#e1bee7', '#d1c4e9', '#c5cae9', '#bbdefb', '#b3e5fc', '#b2ebf2', '#b2dfdb', '#c8e6c9', '#dcedc8', '#f0f4c3', '#fff9c4', '#ffecb3', '#ffe0b2', '#ffccbc'];
const useStyles = makeStyles((theme) => ({
    addIcon: {
        left: '90%',
        top: '85%',
        width: '80px',
        position: 'fixed',
        cursor: 'pointer'
    },
    paper: {
        width: '45%',
        margin: '0px auto',
        padding: '0px auto',
        minHeight: '500px'
    },
    avatarHanded: {
        margin: '5px',
        borderRadius: '50%',
        width: '40px',
        primary: 'lime',
        padding: '12px',
        paddingLeft: '15px',
        fontSize: '18px',
        height: '40px'
    },
    list: {
        width: '50%'
    },
    divider: {
        marginTop: '60px'
    },
}));
const BookAddressList = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [getContactState, setContactState] = useState({ list: false });
    let contacts = [];
    useEffect(() => {
        if (props.show && !getContactState.list) {
            getContactList();
        }
    });
    const selectContact = (contact, uiColor) => {
        contact.uiColor = uiColor;
        let payload = {
            contact: contact
        };
        dispatch(ViewInfo(payload));
        console.log(contact);
    };
    const getContactList = async () => {
        contacts = [];
        const getContactsBookAddress = await Axios.post("/api/getContacts", {});
        console.log(getContactsBookAddress.data);
        getContactsBookAddress.data.table.forEach((contact, index) => {
            const uiColor = uiColors[Math.floor(Math.random() * uiColors.length)];
            contacts.push(
                <Auxiliary key={index} >
                    <ListItem button onClick={() => selectContact(contact, uiColor)}>
                        <div style={{ backgroundColor: uiColor }} className={classes.avatarHanded}>
                            {contact.username[0]}
                        </div>
                        <ListItemText primary={contact.username} secondary={contact.address} />
                    </ListItem>
                    <Divider />
                </Auxiliary>
            );
        });
        console.log(contacts);
        setContactState({ list: contacts });
    };
    useSelector(state => {
        console.log(contacts);
        if (state.ManageBookAddress.needUpdate) {
            let payload = {
                needUpdate: false
            };
            dispatch(UpdateBookAddress(payload));
            getContactList();
        }
    });
    if (getContactState.list) {
        contacts = getContactState.list;
    }
    return (
        <Auxiliary>
            <Paper className={classes.list} style={{ maxHeight: 580, overflow: 'auto' }} >
                <List>
                    {contacts}
                </List>
                <AddCircleOutlineIcon className={classes.addIcon} />
            </Paper>
        </Auxiliary>
    )
};
export default BookAddressList;