import MessageManaging from '../MessageManaging/MessageManaging';
import LoggingAuthentication from '../LoggingAuthentication/LoggingAuthentication';
import SearchManaging from '../SearchManaging/SearchManaging';
import ManageBookAddress from '../ManageBookAddress/ManageBookAddress';
import ManageAutoReply from '../ManageAutoReply/ManageAutoReply';
import {combineReducers} from 'redux';
const allReducers = combineReducers({
    MessageManaging,
    LoggingAuthentication,
    SearchManaging,
    ManageBookAddress,
    ManageAutoReply
});
export default allReducers;