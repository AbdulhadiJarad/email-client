let initialState = {
    email:null,
    password:null
};
const LoggingAuthentication = (state = initialState,action)=>{
    switch (action.type){
        case "LOGIN":
            console.log(action);
            state.email = action.payload.email;
            state.password = action.payload.password;
            return state;
        default: return state;
    }
}
export default LoggingAuthentication;