let initialState = {
    show:false,
    needUpdate: false
};
const ManageBookAddress = (state = initialState,action)=>{
    switch (action.type){
        case "DISPLAY":
            console.log(action);
            state.show = action.payload.show;
            return state;
        case "UPDATE_CHECK":
            state.needUpdate = action.payload.needUpdate;
            return state
        case "VIEW_INFO":
            state.selectedContact = action.payload.contact;
            return state
        default: return state;
    }
}
export default ManageBookAddress;