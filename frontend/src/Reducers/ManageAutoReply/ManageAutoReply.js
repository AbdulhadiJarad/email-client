let initialState = {
    show:false
};
const ManageAutoReply = (state = initialState,action)=>{
    switch (action.type){
        case "DISPLAYAUTO":
            console.log(action);
            state.show = action.payload.showAutoReply;
            return state;
        default: return state;
    }
}
export default ManageAutoReply;