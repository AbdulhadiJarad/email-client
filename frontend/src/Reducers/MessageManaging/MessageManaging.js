let initialState = {

};
const MessageManaging = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
        case "DISPLAY":
            console.log(action);
            state.messageID = action.payload.messageID;
            state.messageId = action.payload.messageId;
            state.messageType = action.payload.messageType;
            state.uid = action.payload.uid;
            state.messageSubject = action.payload.messageSubject;
            state.from = action.payload.from;
            state.replyTo = action.payload.replyTo;
            state.avatarColor = action.payload.avatarColor;
            state.newMessage = false;
            state.date = action.payload.date;
            state.hours = action.payload.hours;
            state.cc = action.payload.cc;
            state.bcc = action.payload.bcc;
            state.to = action.payload.to;
            state.deleted = false;
            return state;
        case "NEW":
            state = {};
            state.newMessage = action.newMessage;
            state.messageId = action.messageId;
            state.replyTo = !action.replyTo?null:action.replyTo;
            state.subject = !action.subject?null:action.subject;
            state.deleted = false;
            return state;
        case "CLEAR":
            let ID = state.messageID;
            state = {
                deleted: action.payload.deleted,
                deletedID: ID
            };
            return state;
        case "SEARCH_DATE":
                state.selectedDate = action.payload.selectedDate;
            return state;
        default: return state;
    }
}
export default MessageManaging;