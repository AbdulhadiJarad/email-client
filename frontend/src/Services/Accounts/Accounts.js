import Axios from '../../../node_modules/axios';
class AccountServices{

    constructor(){
        if (!AccountServices.instance) {
            Services.instance = this;
            this.baseURL = '';
        }
            
        else 
            return this.instance;
    }

    newAccount(){
        return new Promise((resolve, reject) => {
            Axios.get(this.baseURL).then(response => {
                resolve (response);
            }, error => {
                reject(error);
            });
        });
    }

    loginAccount(){
        return new Promise((resolve, reject) => {
            Axios.get(this.baseURL).then(response => {
                resolve (response);
            }, error => {
                reject(error);
            });
        });
    }
}
export default AccountServices;