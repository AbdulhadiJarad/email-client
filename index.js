const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');
var rimraf = require("rimraf");
const simpleParser = require('mailparser').simpleParser;
const app = express();
var Imap = require('imap'), inspect = require('util').inspect;
var fs = require('fs'), fileStream;
const base64 = require("base64-stream");
var inspect = require('util').inspect;
const PORT = process.env.PORT || 3001;
const util = require('util');
const { response, request } = require('express');
const { send } = require('process');
const readdir = util.promisify(fs.readdir);
var readFile = util.promisify(fs.readFile);
var writeFile = util.promisify(fs.writeFile);
app.use(bodyParser.json());
let processStatus = {
    getList: 100,
    needShutOff: true,
    getOfflineList: 100,
    offline: false,
    end: false
};
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.post('/api/newContact', async (request, response) => {

    const requestedContact = request.body.newContact;
    console.log(requestedContact);
    await readFile('./contact/contact.txt', 'utf8');
    const contactFile = await readFile('./contact/contact.txt', 'utf8');
    var contactList = JSON.parse(contactFile);
    contactList.table.forEach((contact, index) => {
        if (contact.username == requestedContact.username) {
            contactList.table[index] = requestedContact;
        }
    });
    await writeFile('./contact/contact.txt', JSON.stringify(contactList));
    response.end(JSON.stringify({ message: 200 }));
});
app.post('/api/AutoReplyManage', async (request, response) => {
    if (request.body.enable) {
        setAutoReply(request, response);
    } else {
        disableAutoReply(request, response);
    }
});
const setAutoReply = async (request, response) => {
    if (!fs.existsSync('./Auto Reply')) {
        fs.mkdirSync("./" + 'Auto Reply');
    }
    console.log('fucking hi');
    console.log(request.body);
    const autoMessage = {
        subject: request.body.subject,
        body: request.body.body
    };
    await writeFile('./Auto Reply/message.txt', JSON.stringify(autoMessage));
    await writeFile('./Auto Reply/enabled.txt', JSON.stringify({ status: true }));
    response.end(JSON.stringify({ message: 200 }));
};

const disableAutoReply = async (request, response) => {
    if (!fs.existsSync('./Auto Reply')) {
        fs.mkdirSync("./" + 'Auto Reply');
    }
    await writeFile('./Auto Reply/enabled.txt', JSON.stringify({ status: false }));
    response.end(JSON.stringify({ message: 200 }));
};
app.post('/api/getAutoReplyData', async (request, response) => {
    if (!fs.existsSync('./Auto Reply')) {
        fs.mkdirSync("./" + 'Auto Reply');
        if (!fs.existsSync('./Auto Reply/message.txt'))
            await writeFile('./Auto Reply/message.txt', '');
        response.end(JSON.stringify({ message: 404 }));
    } else if (fs.existsSync('./Auto Reply')) {
        if (fs.existsSync('./Auto Reply/message.txt')) {
            const data = readFile('./Auto Reply/message.txt', 'utf8');
            if (data.subject || data.body) {
                let enable = readFile('./Auto Reply/enable', 'utf8');
                enable = JSON.parse(enable);
                response.end(JSON.stringify({ message: 200, data: { subject: data.subject, body: data.body }, enable: enable.status }));
            } else {
                response.end(JSON.stringify({ message: 404 }));
            }
        }
    }
    await writeFile('./Auto Reply/enabled.txt', JSON.stringify({ status: false }));
    response.end(JSON.stringify({ message: 200 }));
});
const sendMessage = async (mailOptions) => {
    let userInfo = await readFile('./' + 'User_Info.txt', 'utf8');
    userInfo = JSON.parse(userInfo);
    var transport = nodemailer.createTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        pool: true,
        secure: 'true',
        port: 465,
        auth: {
            user: userInfo.email,
            pass: userInfo.password,
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    transport.sendMail(mailOptions, function (err, res) {
        if (err) {
            console.log('error', err);
            response.end(JSON.stringify({ error: 404 }));
        } else {
            console.log('email sent');
            response.end(JSON.stringify({ message: 200 }));
        }
        transport.close();
    });
};
const handleAutoReply = async (sender) => {
    
    let userInfo = await readFile('./' + 'User_Info.txt', 'utf8');
    if (sender.indexOf(userInfo.email)){
        sender.splice(sender.indexOf(userInfo.email),1);
    }
    if (sender.length>0){
        let autoMessage = await readFile('./Auto Reply/message.txt', 'utf8');
    autoMessage = JSON.parse(autoMessage);
    if (autoMessage.subject || autoMessage.body) {
        var mailOptions = {
            from: userInfo.email,
            to: sender,
            subject: !autoMessage.subject ? 'Auto Reply Message' : autoMessage.subject,
            text: !autoMessage.body ? autoMessage.body : null,
        };
    }
    sendMessage(mailOptions);
    }else {
        console.log('array is empty');
    }
};
app.post('/api/login', (request, response) => {
    // console.log(request.body);
    require('dns').resolve('www.google.com', function (err) {
        if (err) {
            processStatus.offline = true;
            response.end(JSON.stringify({ message: '200' }));
        } else {
            processStatus.offline = false;
        }
    });
    var transport = nodemailer.createTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        pool: true,
        secure: 'true',
        port: 465,
        auth: {
            //put your email to test 
            user: test,
            pass: test,
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    console.log(request.body.email + '123');
    var mailOptions = {
        from: test,
        to: request.body.email,
        subject: 'Welcome !!',
        text: 'Welcome To Our Email Client, We Hope Everthing is Good',
    };
    transport.sendMail(mailOptions, async function (err, res) {
        if (err) {
            console.log(err);
            response.end(JSON.stringify({ message: '404' }));
        } else {
            console.log(request.body.email);
            await writeFile('./' + 'User_Info.txt', JSON.stringify({ email: request.body.email, password: request.body.password }));
            console.log('email sent');
            response.end(JSON.stringify({ message: '200' }));
        }
        transport.close();
    });
});
function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

app.post('/api/sendMessage', async (request, response) => {
    let group = null;
    let validRecepent = request.body.newMessage.to;
    for (let i = 0; i < validRecepent.length; i++) {
        if (!validateEmail(validRecepent[i])) {
            group = validRecepent[i];
            validRecepent.splice(i, 1);
        }
    }

    let readContact = await readFile('./contact/contact.txt', 'utf8');
    readContact = JSON.parse(readContact);
    let recepentGroup = [];
    readContact.table.forEach((contact, index) => {
        contact.groups.map(contactGroup => {
            if (contactGroup == group) {
                recepentGroup.push(contact.address);
            }
        });
    });
    validRecepent = [...validRecepent, ...recepentGroup];
    let userInfo = await readFile('./' + 'User_Info.txt', 'utf8');
    console.log('valid recepent: ', validRecepent);
    var mailOptions = {
        from: userInfo.email,
        to: validRecepent,
        subject: request.body.newMessage.subject,
        inReplyTo: request.body.newMessage.isReplyTo ? request.body.newMessage.messageId : null,
        cc: request.body.newMessage.cc,
        bcc: request.body.newMessage.bcc,
        text: request.body.newMessage.body,
        headers: request.body.newMessage.isReplyTo ? {
            inReplyTo: request.body.newMessage.messageId,
            references: request.body.newMessage.messageId
        } : null
    };
    sendMessage(mailOptions);
});

//open ALL
//Loop On Files
//open Email Message
//Loop On Files
//Read JSON File

async function getContacts(response) {


    const contactFile = await readFile('./contact/contact.txt', 'utf8');

    var json = JSON.parse(contactFile);

    const contactJSON = JSON.stringify(json);

    response.end(contactJSON);

};
app.post('/api/getContacts', (request, response) => {
    getContacts(response);
});
app.post('/api/addContactInfo', async (request, response) => {
    const contactFile = await readFile('./contact/contact.txt', 'utf8');
    var json = JSON.parse(contactFile);
    console.log(request.body);
    json.table.forEach(info => {
        if (info.username == request.body.username) {
            info.phones = request.body.phones;
            info.groups = request.body.groups;
            info.nickname = request.body.nickname;
            info.name = request.body.name;
            return;
        }
    });
    const contactJSON = JSON.stringify(json);
    await writeFile('./contact/contact.txt', contactJSON);
    response.end(JSON.stringify({ message: '200' }));
});
async function readOfflineFiles(response, searchTypeRequest) {
    folderName = (searchTypeRequest[0] == '[' ? searchTypeRequest.replace('[Gmail]/', '') : searchTypeRequest);
    var emailsOfflineData = [];
    const dir = "./" + folderName;
    let directories = await readdir(dir);
    processStatus.getOfflineList = 150;
    const results = await directories.map((file) => {
        return new Promise(async resolve => {
            try {
                try {
                    const uid = await readFile(dir + "/" + file + "/" + "settings.txt", 'utf8');
                    const hi = await readFile(dir + "/" + file + "/" + "configuration.txt", 'utf8');
                    obj = JSON.parse(hi);
                    obj.messageID = file;
                    obj.uid = parseInt(uid);
                    obj.messageType = dir;
                    obj.avatarColor = uiColors[Math.floor(Math.random() * uiColors.length)];
                    if (fs.existsSync(dir + "/" + file + "/" + "attachments")) {
                        obj.hasAttachment = true;
                    }
                    emailsOfflineData.unshift(obj);
                    resolve(obj);
                } catch (err) {
                    resolve(1);
                }
            } catch (err) {
                resolve(1);
            }
        })
    });
    await Promise.all(results).then(() => {
        console.log('Done Fetching Offline Data');
        if (emailsOfflineData) {
            for (let i = 0; i < emailsOfflineData.length; i++) {
                for (let j = i + 1; j < emailsOfflineData.length; j++) {
                    if (emailsOfflineData[i].uid < emailsOfflineData[j].uid) {
                        const temp = Object.assign({}, emailsOfflineData[i]);
                        emailsOfflineData[i] = Object.assign({}, emailsOfflineData[j]);
                        emailsOfflineData[j] = temp;
                    }
                }
            }
        }
        if (processStatus.offline) response.end(JSON.stringify({ emailsOfflineData, status: 200 }));
        processStatus.getList = 100;
        response.end(JSON.stringify({ emailsOfflineData, status: 200 }));
        
    });
};
async function messageBody(searchTypeRequest, request, response) {
    if (searchTypeRequest[0] == '[') {
        searchTypeRequest = searchTypeRequest.replace('[Gmail]/', '');
    } else {
        searchTypeRequest = searchTypeRequest;
    }
    const dir = searchTypeRequest;
    let directories = null;
    try {
        directories = await readdir(dir);
        try {
            let html = true;
            let hi = await readFile(dir + "/" + request.body.messageID + "/" + "body.html", 'utf8');
            if (hi == false || hi == "false" || !hi) {
                hi = await readFile(dir + "/" + request.body.messageID + "/" + "text body.txt", 'utf8');
                html = false;
            }


            let configuration = await readFile(dir + "/" + request.body.messageID + "/" + "configuration.txt", 'utf8');
            let references = JSON.parse(configuration);
            references = configuration.references;
            if (fs.existsSync(dir + "/" + request.body.messageID + '/attachments')) {
                let attachments = [];

                fs.readdir(dir + "/" + request.body.messageID + '/attachments', function (err, filenames) {
                    console.log(dir + "/" + request.body.messageID + '/attachments');
                    return new Promise(async (resolve, reject) => {
                        filenames.forEach(async function (filename) {
                            attachments.push(filename);
                        });
                        resolve(attachments);
                    }).then(async data => {
                        let obj = {
                            html,
                            data: hi,
                            attachments,
                            dir,
                            references
                        };
                        request.body.queryType = 'SEEN';
                        request.body.messageType = searchTypeRequest;
                        deleteMessage(request, null);
                        response.end(JSON.stringify(obj));
                        return;
                    });
                });
            }
            else {
                let obj = {
                    html,
                    data: hi,
                    attachments: [],
                    dir,
                    references
                };
                request.body.queryType = 'SEEN';
                request.body.messageType = searchTypeRequest;
                deleteMessage(request, null);
                response.end(JSON.stringify(obj));
            }

        } catch (err) {

        }
        if (index == directories.length - 1) {

            console.log("done");
        }
    } catch (ex) {

    }
}
const initializeImapObject = async () => {
    let userInfo = await readFile('./' + 'User_Info.txt', 'utf8');
    userInfo = JSON.parse(userInfo);
    return new Imap({
        user: userInfo.email,
        password: userInfo.password,
        host: 'imap.gmail.com',
        port: 993,
        tlsOptions: { rejectUnauthorized: false },
        tls: true
    });
}
const deleteMessage = async (request, response) => {
    console.log(request.body);
    let uid = parseInt(request.body.uid);
    const dir = request.body.dir;
    let directories = null;
    console.log('Message UID: ', uid);
    if (!processStatus.offline) {
        let imap = await initializeImapObject();
        imap.connect();
        async function openInbox(cb) {
            imap.openBox('INBOX', false, cb);
            console.log("open box");
        }
        imap.once('ready', async function () {
            await openInbox(async function (err, box) {
                if (err) throw err;
                // imap.seq.addFlags(1, '\\Deleted', function(err) {console.log("DELETED"); } );
                imap.addFlags([uid], request.body.queryType, async function (err) {
                    if (err) {
                        if (request.body.queryType == 'DELETED') {
                            await response.end(JSON.stringify({ message: 400 }));
                        }
                        console.log('Email Not ' + request.body.queryType);
                    }
                    else {

                        if (request.body.queryType == 'DELETED') {
                            try {
                                rimraf(dir + "/" + request.body.messageID, function () { console.log("done"); });
                                await response.end(JSON.stringify({ message: 200 }));
                                console.log('successfully deleted');
                            } catch (err) {
                                // handle the error
                            }
                        }
                        console.log('Email ' + request.body.queryType);
                    }
                    imap.end();
                });
            });
        });
    }
};

app.post('/api/deleteMessage', async (request, response) => {
    deleteMessage(request, response);
});

function toUpper(thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing; }

function findAttachmentParts(struct, attachments) {
    attachments = attachments || [];
    for (var i = 0, len = struct.length, r; i < len; ++i) {
        if (Array.isArray(struct[i])) {
            findAttachmentParts(struct[i], attachments);
        } else {
            if (struct[i].disposition && ['INLINE', 'ATTACHMENT'].indexOf(toUpper(struct[i].disposition.type)) > -1) {
                attachments.push(struct[i]);
            }
        }
    }
    return attachments;
}
async function getLastUID(dir) {
    let lastUId = '';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync("./" + dir);
    }
    directories = await readdir('./' + dir);
    if (directories.length <= 0) return 0;
    directories.sort(function (a, b) {
        return parseInt(a.match(/\d+/)[0]) - parseInt(b.match(/\d+/)[0]);
    });
    console.log(directories[0], directories[directories.length - 1]);
    lastUId = await readFile('./' + dir + '/' + directories[directories.length - 1] + '/settings.txt');
    console.log('Last UID: ' + lastUId);
    return parseInt(lastUId);
}
function buildAttMessageFunction(attachment, info, imap) {
    var filename = attachment.params.name;
    var encoding = attachment.encoding;
    var dir = info.dir;
    var messageId = info.messageId;
    console.log('./' + dir + messageId + '/attachments/' + filename);
    return function (msg, seqno) {
        var prefix = '(#' + seqno + ') ';
        msg.on('body', function (stream, info) {
            //Create a write stream so that we can stream the attachment to file;
            console.log(prefix + 'Streaming this attachment to file', filename, info);
            var writeStream = fs.createWriteStream('./' + dir + messageId + '/attachments/' + filename);
            writeStream.on('finish', function () {
                console.log(prefix + 'Done writing to file %s', filename);
            });

            //stream.pipe(writeStream); this would write base64 data to the file.
            //so we decode during streaming using 
            if (toUpper(encoding) === 'BASE64') {
                const { Base64Decode } = require('base64-stream');
                //the stream is base64 encoded, so here the stream is decode on the fly and piped to the write stream (file)
                stream.pipe(new Base64Decode()).pipe(writeStream);
            } else {
                //here we have none or some other decoding streamed directly to the file which renders it useless probably
                stream.pipe(writeStream);
            }
        });
        msg.once('end', function () {
            console.log(prefix + 'Finished attachment %s', filename);
        });
    };
}
async function readOnlineEmails(response, searchTypeRequest, searchDate, request) {
    var obj = {
        table: []
    };
    const needRefresh = request.body.refresh;
    let arrayOfAutoRepliment = new Set();
    console.log(searchTypeRequest);
    const searchType = searchTypeRequest;
    let userInfo = await readFile('./' + 'User_Info.txt', 'utf8');
    userInfo = JSON.parse(userInfo);
    var imap = new Imap({
        user: userInfo.email,
        password: userInfo.password,
        host: 'imap.gmail.com',
        port: 993,
        tlsOptions: { rejectUnauthorized: false },
        tls: true
    });
    imap.connect();
    async function openInbox(cb) {
        // [Gmail]/Sent Mail
        // [Gmail]/Drafts
        // [Gmail]/Starred 
        // [Gmail]/Junk
        // [Gmail]/Deleted
        // [Gmail]/Spam
        imap.openBox(searchTypeRequest[0] == '[' ? searchTypeRequest : 'INBOX', false, cb);
        console.log(searchTypeRequest);
        console.log("open box");
    }
    if (searchTypeRequest[0] == '[')
        folderName = searchTypeRequest.replace('[Gmail]/', '');
    else
        folderName = searchTypeRequest;
    console.log('folderName: ' + folderName);
    imap.once('ready', async function () {
        await openInbox(async function (err, box) {
            if (!fs.existsSync('contact')) {
                fs.mkdirSync("./" + 'contact');
                var json = JSON.stringify(obj);
                await writeFile('./contact/contact.txt', json);
                opened = true;
            }
            if (err) throw err;
            // imap.seq.addFlags(1, '\\Deleted', function(err) {console.log("DELETED"); } );
            // ['UID', '689:*']
            // ['SINCE', searchDate ? searchDate : 'JUNE 1, 2020']
            // lastUId==0 && !searchDate? ['SINCE', searchDate ? searchDate : 'JUNE 1, 2020']: ['UID', (lastUId + 1).toString() + ':*']
            let lastUId = 0;
            if (needRefresh) {
                await getLastUID(folderName).then((data) => {
                    lastUId = data;
                });
            }

            console.log('Search Date: ' + searchDate);
            await imap.search(['ALL', (!needRefresh || !lastUId) ? (['SINCE', searchDate ? searchDate : 'July 1, 2020']) : ['UID', (lastUId + 1).toString() + ':*']],
                async function (err, results) {
                    if (!fs.existsSync(folderName)) {
                        fs.mkdirSync("./" + folderName);
                    }
                    var f = await imap.fetch(results,
                        { bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', ''], struct: true }
                    );
                    await f.on('message', async function (msg, seqno) {
                        console.log('Message #%d', seqno);
                        var prefix = ' ' + seqno;
                        await msg.once('body', async function (stream, info) {
                            await simpleParser(stream, 'options', async (err, parsed) => {
                                
                                console.log(seqno);
                                const date = new Date(parsed.date);
                                const dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                                const timeFormat = date.getHours() + ':' + (date.getMinutes()) + ':' + date.getSeconds();
                                let jsonConfig = {
                                    headers: parsed.headers,
                                    subject: parsed.subject,
                                    avatarColor: uiColors[Math.floor(Math.random() * uiColors.length)],
                                    from: parsed.from,
                                    to: parsed.to,
                                    cc: parsed.cc,
                                    messageType: searchType,
                                    bcc: parsed.bcc,
                                    date: dateFormat,
                                    hours: timeFormat,
                                    messageId: parsed.messageId,
                                    inReplyTo: parsed.inReplyTo,
                                    replyTo: parsed.replyTo,
                                    references: parsed.references,
                                    text: parsed.text,
                                    messageID: 'Email Message ' + prefix,
                                    messageType: folderName
                                };
                                if (!fs.existsSync('./' + folderName + '/Email Message ' + prefix)) {
                                    fs.mkdirSync("./" + folderName + "/Email Message " + prefix);
                                }
                                try {
                                    let contact = {
                                        "username": parsed.from.value[0].name,
                                        "address": parsed.from.value[0].address,
                                        "name": "",
                                        "nickname": "",
                                        "phones": [],
                                        "groups": []
                                    };
                                    await writeFile('./' + folderName + '/Email Message ' + prefix + '/body.html', parsed.html);
                                    await writeFile("./" + folderName + "/Email Message " + prefix + '/text body.txt', parsed.text);
                                    await writeFile("./" + folderName + "/Email Message " + prefix + '/configuration.txt', JSON.stringify(jsonConfig));
                                    // await writeFile('./' + folderName + '/Email Message ' + prefix + '/HasBeenAutoReplied.txt', JSON.stringify({ status: false }));
                                    const contactFile = await readFile('./contact/contact.txt', 'utf8');
                                    if (folderName == 'ALL') {
                                        
                                        if (fs.existsSync('./Auto Reply')) {
                                          
                                            if (fs.existsSync('./Auto Reply/enabled.txt') && fs.existsSync('./Auto Reply/message.txt')) {
                                                let enable = await readFile('./Auto Reply/enabled.txt', 'utf8');
                                                enable = JSON.parse(enable);
                                                
                                                if (enable.status) {
                                                    let message = await readFile('./Auto Reply/message.txt', 'utf8');
                                                    message = JSON.parse(message);
                                                    
                                                    if (message.subject || message.body) {
                                                        console.log('hi abdulhadi');
                                                        if (fs.existsSync('./' + folderName + '/Email Message ' + prefix + '/HasBeenAutoReplied.txt')) {
                                                            let hassBeenReplied = await readFile('./' + folderName + '/Email Message ' + prefix + '/HasBeenAutoReplied.txt', 'utf8');
                                                            hassBeenReplied = JSON.parse(hassBeenReplied);
                                                            if (!hassBeenReplied.status) {
                                                                await writeFile('./' + folderName + '/Email Message ' + prefix + '/HasBeenAutoReplied.txt', JSON.stringify({ status: true }));
                                                                arrayOfAutoRepliment.add(parsed.from.value[0].address);
                                                            } else {
                                                                console.log('you have already send auto message to: ' + parsed.from.value[0].address);
                                                            }
                                                        } else {
                                                            await writeFile('./' + folderName + '/Email Message ' + prefix + '/HasBeenAutoReplied.txt', JSON.stringify({ status: true }));
                                                            arrayOfAutoRepliment.add(parsed.from.value[0].address);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    obj = JSON.parse(contactFile);
                                    let exist = false;
                                    for (let i = 0; i < obj.table.length; i++) {
                                        if (obj.table[i].username == contact.username) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                    if (!exist) {
                                        console.log("write for: ", contact.username);
                                        obj.table.push(contact);
                                        var json = JSON.stringify(obj);
                                        await writeFile("./contact/contact.txt", json);
                                    }
                                    console.log("Done");

                                } catch (ex) {

                                }
                            });
                        });
                        msg.once('attributes', async function (attrs) {
                            console.log('./' + folderName + '/Email Message ' + prefix,!fs.existsSync('./' + folderName + '/Email Message ' + prefix ))
                            if (!fs.existsSync('./' + folderName + '/Email Message ' + prefix )) {
                                console.log('./' + folderName + '/Email Message ' + prefix);
                                fs.mkdirSync('./' + folderName + '/Email Message ' + prefix);
                                await writeFile('./' + folderName + '/Email Message ' + prefix + '/settings.txt', [attrs.uid]);
                            }
                            else {
                                await writeFile('./' + folderName + '/Email Message ' + prefix + '/settings.txt', [attrs.uid]);
                            }
                            var attachments = findAttachmentParts(attrs.struct);
                            console.log(prefix + 'Has attachments: %d', attachments.length);
                            if (attachments.length > 0 && !fs.existsSync("./" + folderName + "/Email Message " + prefix + '/attachments')) {
                                fs.mkdirSync("./" + folderName + "/Email Message " + prefix + '/attachments');
                            }
                            for (var i = 0, len = attachments.length; i < len; ++i) {
                                var attachment = attachments[i];
                                console.log(prefix + 'Fetching attachment %s', attachment.params.name);
                                var f = imap.fetch(attrs.uid, { //do not use imap.seq.fetch here
                                    bodies: [attachment.partID],
                                    struct: true
                                });
                                //build function to process attachment message

                                let info = {
                                    dir: folderName,
                                    messageId: '/Email Message ' + prefix
                                };
                                await f.on('message', buildAttMessageFunction(attachment, info, imap));
                            }
                        });
                        msg.once('end', async function () {
                            console.log('message end ');
                        });
                    });
                    f.once('end', async function () {
                        console.log('Done fetching all messages!');
                        processStatus.getList = 200;
                        // imap.end();
                    });
                    f.once('error', function (err) { console.log('Fetch error: ' + err); });
                });
        });
    });
    imap.once('error', function (err) { console.log(err); });
    imap.once('end', function () {
        console.log('Online Connection ended');
        if (arrayOfAutoRepliment.size > 0) {
            let array = [...arrayOfAutoRepliment];
            console.log(array);
            handleAutoReply(array);
        } else {
            console.log('array is empty')
        }
        processStatus.getList = 200;
    });
}
const uiColors = ['#f8bbd0', '#ffcdd2', '#f8bbd0', '#e1bee7', '#d1c4e9', '#c5cae9', '#bbdefb', '#b3e5fc', '#b2ebf2', '#b2dfdb', '#c8e6c9', '#dcedc8', '#f0f4c3', '#fff9c4', '#ffecb3', '#ffe0b2', '#ffccbc'];

app.post('/api/readBody', (request, response) => {
    const searchTypeRequest = request.body.searchType;
    messageBody(searchTypeRequest, request, response);
});

app.post('/api/getList', (request, response) => {
    const searchTypeRequest = request.body.searchType;
    const searchDate = request.body.searchDate;
    require('dns').resolve('www.google.com', function (err) {
        if (err) {
            processStatus.offline = true;
            readOfflineFiles(response, searchTypeRequest);
            console.log("No connection");
        } else {
            processStatus.offline = false;
            console.log(processStatus.getList);
            if (processStatus.getList == 100) {
                processStatus.getList = 150;
                response.end(JSON.stringify({ status: '100' }));
                readOnlineEmails(response, searchTypeRequest, searchDate, request);
            } else if (processStatus.getList == 150) {
                response.end(JSON.stringify({ status: '150' }));
            } else if (processStatus.getList == 200) {
                readOfflineFiles(response, searchTypeRequest, request);
            }
            console.log("Connected");
        }
    });
});
app.listen(PORT, () => { console.log('server listening on port ', PORT); });